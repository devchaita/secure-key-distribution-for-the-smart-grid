package Controllers;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

import utilities.AESEncryption;
import utilities.H2;
import utilities.HMAC_sha512;
import utilities.IDGenerator;
import utilities.RSAEnryption;

public class SmartMeter {

	public static void main(String[] args) {
		// Generate Smart meter ID
		IDGenerator smartMeterID = new IDGenerator();
		UUID IDm=smartMeterID.getID();
		System.out.println("Smart Meter ID:" + IDm.toString());
		
		//Generate Service Provider ID
		IDGenerator serviceProviderID = new IDGenerator();
		UUID IDs=serviceProviderID.getID();
		System.out.println("Service Provider ID:" +  IDs.toString());
		
		
		/**
		 *  Write a stub to send IDs to TrustAnchor
		 *  
		 */
		
		//Generate a pseudorandom number for association with IDm
		Random randomGenerator = new Random();
		int r = randomGenerator.nextInt(10000);
		System.out.println("Random number generated is:" + r );
		
		
		Map rToIDm = new HashMap();
		rToIDm.put(IDm, r);
		
		//Generate x use HMAC and a common key which is "SecureKeyExchange"
		HMAC_sha512 hmac_sha512 = new HMAC_sha512();
		String x=hmac_sha512.getHMAC_sha512(IDm.toString(), "SecureKeyExchange");
		
		//Generate the km
		RSAEnryption rsaEncryption = new RSAEnryption();
		
	    String km=rsaEncryption.encrypt(x, IDm.toString());

	     
		/**
		 * Write a code to receive the Encrypted Message from TrustAnchor
		 * 
		 */
		String encryptedText= null;
		
		/**
		 * Write a code to receive beta and s from Service Provider
		 */
		int s=0;
		String beta=null;
	    //Decrypt the message using km as key
	    AESEncryption aesEncryption = new AESEncryption();
	    String k=aesEncryption.decrypt(encryptedText, km);
	    
	    
	    //Compute H2
	    H2 h2= new H2();
	    String betaprime=h2.sessionKeyOmega(k, String.valueOf(r), String.valueOf(s), String.valueOf(0));
	    
	    if (beta.equals(betaprime))
	    {
	    	//accept the setting and generate session key omega
	    	String omega=h2.sessionKeyOmega(k, String.valueOf(s), IDm.toString(), IDs.toString());
	    	
	    }
		//generate delta
	    String delta = h2.sessionKeyOmega(k, String.valueOf(s), String.valueOf(1), null);
	    //send delta to Service Provider
	    
	}

}
