package Controllers;

import utilities.AESEncryption;
import utilities.HMAC_sha512;
import utilities.RSAEnryption;
import utilities.WHIRLPOOLEncryption;

public class TrustAnchor {
	public static void main(String args[])
	{
		
		String IDm=null;
		String IDs=null;
		/**
		 * Write a stub to receive IDm, IDs from Smart Meter
		 */
		//Generate x
		HMAC_sha512 hmac_sha512 = new HMAC_sha512();
		String x=hmac_sha512.getHMAC_sha512(IDm.toString(), "SecureKeyExchange");
		
		//Generate the km
		RSAEnryption rsaEncryption = new RSAEnryption();
				
	    String km=rsaEncryption.encrypt(x, IDm);

		//Generate the ks
	    
	 	
	    String ks=rsaEncryption.encrypt(x, IDs);
	    
	    //Compute fks
	    WHIRLPOOLEncryption whirlpoolEncryption = new WHIRLPOOLEncryption();
		String fks=whirlpoolEncryption.encrypt(ks, IDm);
		
		//Encrypt fks usig AES and km as key
		
		AESEncryption aesEncryption = new AESEncryption();
	    String epsilon=aesEncryption.encrypt(km, fks);
		
	    /**
	     * Write a code to send the epsilon to SmartMeter
	     */
	    
	    /**
	     * Write a code to send ks to Service Provider
	     */

		
	}

}
