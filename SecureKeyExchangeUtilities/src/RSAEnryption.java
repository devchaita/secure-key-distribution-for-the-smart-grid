import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.crypto.Cipher;

import org.apache.commons.codec.binary.Base64;
public class RSAEnryption {
	public String enccrypt(String key, String plaintext)
	{
	KeyPairGenerator keyPairGenerator = null;
	try {
		keyPairGenerator = KeyPairGenerator.getInstance("RSA");
	} catch (NoSuchAlgorithmException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
    keyPairGenerator.initialize(1024);
    KeyPair keyPair = keyPairGenerator.genKeyPair();

    Cipher cipher = null;
    byte[] cipherText=null;
    String encodedcipher=null;	
	try {
		cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
	} catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
    try {
		cipher.init(Cipher.ENCRYPT_MODE, keyPair.getPublic());
	} catch (InvalidKeyException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
    try {
		 cipherText = cipher.doFinal(plaintext.getBytes());
		  encodedcipher=Base64.encodeBase64String(cipherText);
	
	} catch (IllegalBlockSizeException | BadPaddingException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	return encodedcipher;
    }

	
}
