package utilities;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.codec.binary.Base64;

import jonelo.jacksum.*;
import jonelo.jacksum.algorithm.*; 
public class WHIRLPOOLEncryption
{

	public static String encrypt(String key, String message)
	{
		AbstractChecksum whirlpoolchecksum = null;
		   try {
		     // select an algorithm (md5 in this case)
			   whirlpoolchecksum = JacksumAPI.getChecksumInstance("whirlpool0");
			   whirlpoolchecksum.update(message.getBytes());
		     return whirlpoolchecksum.getFormattedValue();
		     // On some systems you get a better performance for particular
		     // algorithms if you select an alternate algorithm (see also option -A)
		     // checksum = JacksumAPI.getChecksumInstance("md5", true);
		   } catch (NoSuchAlgorithmException nsae) {
		     // algorithm doesn't exist
		   }
		return message;

	}




}
