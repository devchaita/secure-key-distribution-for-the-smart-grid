package utilities;
import java.security.InvalidKeyException;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

public class HMAC_sha512
{

	public String getHMAC_sha512(String plaintext,String key)
	{
		try
		{
			Mac sha512_HMAC = Mac.getInstance("HMAC/SHA512");
			SecretKeySpec secret_key = new SecretKeySpec(key.getBytes(), "HmacSHA512");
			sha512_HMAC.init(secret_key);
 			
			String hash=Base64.encodeBase64String(sha512_HMAC.doFinal(plaintext.getBytes()));
			return hash;

		}
		catch(java.security.NoSuchAlgorithmException | InvalidKeyException e)
		{
			System.out.println("Exception is "+ e.getMessage());
		}
		return key;	
	}




}
